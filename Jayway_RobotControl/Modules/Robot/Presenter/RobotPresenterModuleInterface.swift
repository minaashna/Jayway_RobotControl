//
//  RobotPresenterModuleInterface.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/11/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

protocol RobotPresenterModuleInterface: class {
    
    /// Moving the robot by executing the given command
    ///
    /// - Parameters:
    ///   - environment: Required properties to create an environment
    ///   - position: Required properties to create initial position
    ///   - command: Command string to move the robot
    func executeCommand(in environment: (maxColumn: String?, maxRow: String?),
                        at position: (column: String?, row: String?, direction: String?),
                        with command: String?) 
}
