//
//  RobotPresenter.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 11/4/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

/// Robot Presenter Error
///
/// - invalidMovement: The command order is not valid within the environment
/// - invalidCommand: The user input characters for command is not valid.
enum RobotPresenterError: Error {
    case invalidMovement
    case invalidCommand
    case invalidPosition
}

// MARK: - LocalizedError
extension RobotPresenterError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidCommand:
            return "The input command characters are not valid."
        case .invalidMovement:
            return "The command order is not valid within the environment."
        case .invalidPosition:
            return "Position is not valid in the environment."
        }
    }
}

class RobotPresenter {
    var environment: Environment!
    var position: Position!
    var robot: Robot!
    
    weak var userInterface: RobotControllerModuleInterface?
    
    init(userInterface: RobotControllerModuleInterface) {
        self.userInterface = userInterface
    }

}

// MARK: - Private Methods
extension RobotPresenter {
    
    /// Moving the robot
    ///
    /// - Parameters:
    ///   - robot: the robot instance
    ///   - position: the robot current position
    ///   - environment: the environment which the robot is in
    ///   - command: the command which the robot has to move based on it.
    /// - Returns: the robot new position after movement
    /// - Throws: Invalid commandInput or movement outside the environment
    private func move(robot: Robot,to position: Position,
                      in environment: Environment,
                      with command: String?) throws -> Position {
        
        /// if command is empty robot has to return it's current position
        guard let command = command else {
            return robot.position
        }
    
        try command.filter { char in
            guard (char == "R" || char == "L" || char == "F") else {
                throw RobotPresenterError.invalidCommand
            }
             return true
            }.forEach { char in
            if char == "R" {
                robot.turnRight()
            }
            
            if char == "L" {
                robot.turnLeft()
            }
            
            if char == "F" {
                guard robot.moveForward().isPositionValid(in: self.environment) else {
                    throw RobotPresenterError.invalidMovement
                }
            }
        }
        
        return robot.position
    }

}

// MARK: - RobotPresenterModuleInterface
extension RobotPresenter: RobotPresenterModuleInterface {
    func executeCommand(in environment: (maxColumn: String?, maxRow: String?),
                        at position: (column: String?, row: String?, direction: String?),
                        with command: String?) {
        var resultError: Error?
        var resultPosition: Position?
        defer {
            if let resultError = resultError  {
                userInterface?.show(error: resultError)
            } else if let resultPosition = resultPosition {
                userInterface?.show(result: resultPosition)
            }
        }
        
        do {
            self.environment = try Environment(maxColumn: environment.maxColumn,
                                          maxRow: environment.maxRow)
            self.position = try Position(column: position.column,
                                         row: position.row,
                                         direction: position.direction)
            guard self.position.isPositionValid(in: self.environment) else {
                throw RobotPresenterError.invalidPosition
            }
            self.robot = Robot(position: self.position)
            let result = try move(robot: robot,
                                  to: self.position,
                                  in: self.environment,
                                  with: command)
            resultPosition = result
        } catch {
            resultError = error
            return
        }
    }

}
