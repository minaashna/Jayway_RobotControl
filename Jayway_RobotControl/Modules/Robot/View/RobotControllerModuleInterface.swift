//
//  RobotControllerModuleInterface.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/11/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

protocol RobotControllerModuleInterface: class {
    
    /// Showing the movement result on the screen
    ///
    /// - Parameter result: the final position of the robot
    func show(result: Position)
    
    /// Showing the error on the screen
    ///
    /// - Parameter error: the error which is occured during the process
    func show(error: Error)
}
