//
//  RobotController.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/10/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class RobotController: UIViewController {

    @IBOutlet weak var maxRowTextField: UITextField!
    @IBOutlet weak var columnTextField: UITextField!
    @IBOutlet weak var maxColumnTextField: UITextField!
    @IBOutlet weak var rowTextField: UITextField!
    @IBOutlet weak var directionTextField: UITextField!
    @IBOutlet weak var commandTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var presenter: RobotPresenterModuleInterface?
}


// MARK: - Override Methods
extension RobotController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// MARK: - Actions
extension RobotController {
    @IBAction func submit(_ sender: UIBarButtonItem) {
        let environment = (maxColumnTextField.text, maxRowTextField.text)
        let position = (columnTextField.text, rowTextField.text, directionTextField.text)
        let command = commandTextField.text
        presenter?.executeCommand(in: environment,
                                  at: position,
                                  with: command)
    }
}

// MARK: - RobotControllerModuleInterface
extension RobotController: RobotControllerModuleInterface {
    func show(result: Position) {
        resultLabel.text = "Result: \(result.column, result.row, result.direction.string)"
    }
    
    func show(error: Error) {
        resultLabel.text = "Error: \(error.localizedDescription)"
    }

}
