//
//  Environment.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/10/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

// MARK: Environment Error
enum EnvironmentError: Error {
    case invalidSize
    case invaldInputs
}

// MARK: - LocalizedError
extension EnvironmentError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invaldInputs:
            return "Environment Invalid Input."
        case .invalidSize:
            return "Environment Invalid Size."
        }
    }
}

class Environment {
    private(set) var maxColumn: Int!
    private(set) var maxRow: Int!
    
    init(maxColumn: String?, maxRow: String?) throws {
        guard let inputResult = validateEnvironmentInput(maxColumn: maxColumn, maxRow: maxRow) else {
            throw EnvironmentError.invaldInputs
        }
        self.maxColumn = inputResult.maxColumn
        self.maxRow = inputResult.maxRow
        guard isEnvironmentSizeValid() else {
            throw EnvironmentError.invalidSize
        }
    }
}

// MARK: - Private Methods
extension Environment {
    
    /// Validating environment size based on user input(Negative numbers are not allowed)
    ///
    /// - Returns: whether the environment size is valid or not
    private func isEnvironmentSizeValid() -> Bool {
        guard maxRow > 0 && maxColumn > 0 else {
            return false
        }
        return true
    }
    
    /// Validating the user input values
    ///
    /// - Parameters:
    ///   - maxColumn: maximum number of columns
    ///   - maxRow: maximum number of rows
    /// - Returns: the valid max column number and row
    private func validateEnvironmentInput(maxColumn: String?, maxRow: String?) -> (maxColumn: Int,maxRow: Int)? {
        guard let maxColumnValue = maxColumn,
            let maxColumnIntValue = Int(maxColumnValue),
            let maxRowValue = maxRow,
            let maxRowIntValue = Int(maxRowValue) else {
                return nil
        }
        
        return (maxColumnIntValue, maxRowIntValue)
    }
}
