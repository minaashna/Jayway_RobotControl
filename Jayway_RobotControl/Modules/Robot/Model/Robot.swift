//
//  Robot.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/10/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class Robot {
    private(set) var position: Position
    
    init(position: Position) {
        self.position = position
    }
}

// MARK: - Public Methods
extension Robot {
    
    /// Changing the robot direction by turning right
    func turnRight() {
        position.direction = Direction(with: position.direction.rawValue + 1)
    }
    
    /// Changing the robot direction by turning left
    func turnLeft() {
        position.direction = Direction(with: position.direction.rawValue - 1)
    }
    
    /// Changing the robot position by moving it one square forward
    ///
    /// - Returns: The robot new position
    func moveForward() -> Position {
        var nextPosition = position
        
        switch position.direction {
        case .north:
            nextPosition.row = nextPosition.row - 1
        case .east:
            nextPosition.column = nextPosition.column + 1
        case .west:
            nextPosition.column = nextPosition.column - 1
        case .south:
            nextPosition.row = nextPosition.row + 1
        default: break
        }
        
        position = nextPosition
        return nextPosition
    }
}
