//
//  Position.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/10/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

/// Position Error
///
/// - invalidPosition: The position is not valid in the environment
/// - invalidPositionInputs: The user inputs values for position is not valid(invalid characters)
enum PositionError: Error {
    case invalidPositionInputs
}

// MARK: - LocalizedError
extension PositionError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidPositionInputs:
            return "Position inputs are not valid."
        }
    }
}

struct Position {
    var row: Int!
    var column: Int!
    var direction: Direction!

    init(column: String?, row: String?, direction: String?) throws {
        guard let inputResult = validatePositionInputs(column: column, row: row, direction: direction) else {
            throw PositionError.invalidPositionInputs
        }
        self.column = inputResult.column
        self.row = inputResult.row
        self.direction = inputResult.direction
    }
}

// MARK: - Private Methods
extension Position {

    /// Validating the user input values
    ///
    /// - Parameters:
    ///   - column: initial column
    ///   - row: initial row
    ///   - direction: initial direction
    /// - Returns: the valid column, row and direction
    private func validatePositionInputs(column: String?, row: String?, direction: String?) -> (column: Int, row: Int, direction: Direction)? {
        guard let columnValue = column, let columnIntValue = Int(columnValue),
            let rowValue = row, let rowIntValue = Int(rowValue),
            let directionValue = direction else {
                return nil
        }
        let directionRawValue = Direction(with: directionValue)
        guard directionRawValue != Direction.unknown else {
            return nil
        }
        return (columnIntValue, rowIntValue, directionRawValue)
    }
}

// MARK: - Public Methods
extension Position {
    
    /// Checking whether the position is valid in the environment
    ///
    /// - Parameter environment: the environment which the position should be validate in
    /// - Returns: validation result (true for valid, false for invalid)
    func isPositionValid(in environment: Environment) -> Bool {
        guard self.row >= 0 && self.row < environment.maxRow else {
            return false
        }
        guard self.column >= 0 && self.column < environment.maxColumn else {
            return false
        }
        return true
    }
}
