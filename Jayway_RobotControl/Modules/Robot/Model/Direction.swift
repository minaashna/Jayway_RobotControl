//
//  Direction.swift
//  Jayway_RobotControl
//
//  Created by Mina Ashena on 4/11/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

/// The robot direction
///
/// - north: Head to north
/// - east: Head to east
/// - south: Head to south
/// - west: Head to west
/// - unknown: direction is unknown
enum Direction: Int {
    case north
    case east
    case south
    case west
    case unknown
    
    /// Initialize a Direction with an Integer value
    ///
    /// - Parameter value: initial value
    init(with value: Int) {
        /// If the initial value is less than north rawValue(0) the direction will be west
        /// If the initial value is greater than west rawValue(3), the direction will be north
        if value < Direction.north.rawValue {
            self = .west
            return
        } else if value > Direction.west.rawValue {
            self = .north
            return
        }
        guard let direction = Direction(rawValue: value) else {
            self = .unknown
            return
        }
        self = direction
    }
    
    /// Initialize a direction with an string value(user input)
    ///
    /// - Parameter value: initital value
    init(with value: String) {
        switch value.uppercased() {
        case Direction.north.string:
            self = .north
        case Direction.east.string:
            self = .east
        case Direction.west.string:
            self = .west
        case Direction.south.string:
            self = .south
        default:
            self = .unknown
        }
    }
}

extension Direction {
    
    /// Converting the direction value to string
    var string: String {
        switch self {
        case .north:
            return "N"
        case .east:
            return "E"
        case .west:
            return "W"
        case .south:
            return "S"
        default:
            return "Unknown"
        }
    }
}
