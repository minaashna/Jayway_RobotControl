//
//  EnvironmentTests.swift
//  Jayway_RobotControlTests
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import XCTest
@testable import Jayway_RobotControl

class EnvironmentTests: XCTestCase {
    var sut: Environment?
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testInitializeWithCorrectValue() {
        XCTAssertNoThrow(sut = try Environment(maxColumn: "5", maxRow: "5"))
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut?.maxColumn, 5)
        XCTAssertEqual(sut?.maxRow, 5)
    }
    
    func testInitializeWithInvalidInputValue() {
        XCTAssertThrowsError(try Environment(maxColumn: "T", maxRow: "N"), "expected error") { (error) in
            XCTAssertEqual(error as? EnvironmentError, EnvironmentError.invaldInputs)
        }
    }

    func testInitializeWithInvalidSizeValue() {
        XCTAssertThrowsError(try Environment(maxColumn: "-5", maxRow: "-5"), "expected error") { (error) in
            XCTAssertEqual(error as? EnvironmentError, EnvironmentError.invalidSize)
        }
    }
}
