//
//  PositionTests.swift
//  Jayway_RobotControlTests
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import XCTest
@testable import Jayway_RobotControl

class PositionTests: XCTestCase {
    var sut: Position?
    var environment: Environment?
    
    override func setUp() {
        super.setUp()
        environment = try? Environment(maxColumn: "5", maxRow: "5")
    }
    
    override func tearDown() {
        sut = nil
        environment = nil
        super.tearDown()
    }

    func testPositionWithCorrectInputValue() {
        XCTAssertNoThrow(sut = try Position(column: "1", row: "2", direction: "N"))
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut?.column, 1)
        XCTAssertEqual(sut?.row, 2)
        XCTAssertEqual(sut?.direction, Direction.north)
    }

    func testPositionWithInvalidColumnValue() {
        XCTAssertThrowsError(try Position(column: "T", row: "2", direction: "N"), "expected error") { (error) in
            XCTAssertEqual(error as? PositionError, PositionError.invalidPositionInputs)
        }
    }
    
    func testPositionWithInvalidRowValue() {
        XCTAssertThrowsError(try Position(column: "1", row: "T", direction: "N"), "expected error") { (error) in
            XCTAssertEqual(error as? PositionError, PositionError.invalidPositionInputs)
        }
    }
    
    func testPositionWithInvalidDirection() {
        XCTAssertThrowsError(try Position(column: "1", row: "2", direction: "T"), "expected error") { (error) in
            XCTAssertEqual(error as? PositionError, PositionError.invalidPositionInputs)
        }
    }
    
    func testIsPositionValidWithCorrectValue() {
        XCTAssertNoThrow(sut = try Position(column: "1", row: "2", direction: "N"))

        let result = sut?.isPositionValid(in: environment!)

        XCTAssertTrue(result!)
    }

    func testIsPositionValidWithRowIsLessThanZero() {
        XCTAssertNoThrow(sut = try Position(column: "1", row: "-2", direction: "N"))

        let result = sut?.isPositionValid(in: environment!)

        XCTAssertFalse(result!)
    }

    func testIsPositionValidWithRowIsGreaterThanEnvironmentMaxRowValue() {
        XCTAssertNoThrow(sut = try Position(column: "1", row: "\(environment!.maxRow + 1)", direction: "N"))

        let result = sut?.isPositionValid(in: environment!)

        XCTAssertFalse(result!)
    }

    func testIsPositionValidWithColumnIsLessThanZero() {
        XCTAssertNoThrow(sut = try Position(column: "-1", row: "2", direction: "N"))

        let result = sut?.isPositionValid(in: environment!)

        XCTAssertFalse(result!)
    }

    func testIsPositionValidWithColumnIsGreaterThanEnvironmentMaxColumnValue() {
        XCTAssertNoThrow(sut = try Position(column: "\(environment!.maxColumn + 1)", row: "2", direction: "N"))

        let result = sut?.isPositionValid(in: environment!)

        XCTAssertFalse(result!)
    }
}
