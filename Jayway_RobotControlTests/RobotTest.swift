//
//  RobotTest.swift
//  Jayway_RobotControlTests
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import XCTest
@testable import Jayway_RobotControl

class RobotTest: XCTestCase {
    var sut: Robot?
    var position: Position?
    
    override func setUp() {
        super.setUp()
        position = try? Position(column: "1", row: "2", direction: "N")
    }
    
    override func tearDown() {
        sut = nil
        position = nil
        super.tearDown()
    }
    
    func testRobotWithCorrectValue() {
        sut = Robot(position: position!)
        
        XCTAssertEqual(sut?.position.column, position?.column)
        XCTAssertEqual(sut?.position.row, position?.row)
        XCTAssertEqual(sut?.position.direction, position?.direction)
    }
    
    func testTurnRightFromNorth() {
        sut = Robot(position: position!)
        
        sut?.turnRight()
        
        XCTAssertEqual(sut?.position.direction, Direction.east)
    }
    
    func testTurnLeftFromNorth() {
        sut = Robot(position: position!)
        
        sut?.turnLeft()
        
        XCTAssertEqual(sut?.position.direction, Direction.west)
    }
    
    func testMoveForwardFromNorth() {
        sut = Robot(position: position!)

        let nextPosition = sut?.moveForward()
        
        XCTAssertEqual(nextPosition!.row, position!.row - 1)
        XCTAssertEqual(sut?.position.column, nextPosition?.column)
        XCTAssertEqual(sut?.position.row, nextPosition?.row)
        XCTAssertEqual(sut?.position.direction, nextPosition?.direction)
    }
    
}
