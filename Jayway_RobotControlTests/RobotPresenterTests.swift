//
//  RobotPresenterTests.swift
//  Jayway_RobotControlTests
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import XCTest
@testable import Jayway_RobotControl

class MockUserInterface: RobotControllerModuleInterface {
    private(set) var resultPosition: Position?
    private(set) var resultError: Error?
    
    func show(result: Position) {
        resultPosition = result
    }
    
    func show(error: Error) {
        resultError = error
    }
}

class RobotPresenterTests: XCTestCase {
    var sut: RobotPresenter?
    var mockUserInterface: MockUserInterface?
    
    override func setUp() {
        super.setUp()
        mockUserInterface = MockUserInterface()
        sut = RobotPresenter(userInterface: mockUserInterface!)
    }
    
    override func tearDown() {
        mockUserInterface = nil
        sut = nil
        super.tearDown()
    }
    
    func testExecuteCommandWithCorrectValue() {
        let expectedPosition = try? Position(column: "1", row: "3", direction: "N")
        sut?.executeCommand(in: (maxColumn: "5", maxRow: "5"),
                            at: (column: "1", row: "2", direction: "N"),
                            with: "RFRFFRFRF")
        
        XCTAssertNotNil(mockUserInterface?.resultPosition)
        XCTAssertEqual(mockUserInterface?.resultPosition?.column, expectedPosition?.column)
        XCTAssertEqual(mockUserInterface?.resultPosition?.row, expectedPosition?.row)
        XCTAssertEqual(mockUserInterface?.resultPosition?.direction, expectedPosition?.direction)
    }
    
    func testExecuteCommandWithInvalidEnvironment() {
        sut?.executeCommand(in: (maxColumn: "-5", maxRow: "5"),
                            at: (column: "1", row: "2", direction: "N"),
                            with: "RFRFFRFRF")
        
        XCTAssertNotNil(mockUserInterface?.resultError)
        XCTAssertEqual(mockUserInterface?.resultError as? EnvironmentError, EnvironmentError.invalidSize)
    }
    
    func testExecuteCommandWithInvalidPosition() {
        sut?.executeCommand(in: (maxColumn: "5", maxRow: "5"),
                            at: (column: "10", row: "2", direction: "N"),
                            with: "RFRFFRFRF")
        
        XCTAssertNotNil(mockUserInterface?.resultError)
        XCTAssertEqual(mockUserInterface?.resultError as? RobotPresenterError, RobotPresenterError.invalidPosition)
    }
    
    func testExecuteCommandWithInvalidCommandInput() {
        sut?.executeCommand(in: (maxColumn: "5", maxRow: "5"),
                            at: (column: "1", row: "2", direction: "N"),
                            with: "RFRFFRFRFMH")
        
        XCTAssertNotNil(mockUserInterface?.resultError)
        XCTAssertEqual(mockUserInterface?.resultError as? RobotPresenterError, RobotPresenterError.invalidCommand)
    }
    
    func testExecuteCommandWithInvalidMovement() {
        sut?.executeCommand(in: (maxColumn: "5", maxRow: "5"),
                            at: (column: "1", row: "2", direction: "N"),
                            with: "RFRFFRFF")
        
        XCTAssertNotNil(mockUserInterface?.resultError)
        XCTAssertEqual(mockUserInterface?.resultError as? RobotPresenterError, RobotPresenterError.invalidMovement)
    }
}
